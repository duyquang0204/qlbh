// Kiểm tra vadation các giá trị nhập vào

//Kiểm tra giá trị rỗng nhập vào

function kiemTraRong(value, selectorError, name) {
  if (value === "") {
    document.querySelector(selectorError).innerHTML =
      name + " không được bỏ trống";
    return false;
  }
  {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
}

//Kiểm tra ký tự

function kiemTraKiTu(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z]*$/;
  if (regexLetter.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  } else {
    document.querySelector(selectorError).innerHTML =
      name + " phải nhập là chữ cái";
    return false;
  }
}

// Kiểm tra number
function kiemTraNumber(value, selectorError, name) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  {
    document.querySelector(selectorError).innerHTML =
      name + " phải nhập chữ số";
    return false;
  }
}
