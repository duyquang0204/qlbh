//Phần thông tin nội dung sản phẩm

let sanPham = new SanPhamService();
// console.log("sanPhamSer: ", sanPhamSer);

//Hàm rút gọn cú pháp document.getElements..
// function getELE(id) {
//   return document.getElementById(id);
// }

getListProducts();
function getListProducts() {
  sanPham
    .layDSSP()
    .then(function (result) {
      // console.log(result.data);
      renderTable(result.data);
      setLocalStorage(result.data);
    })
    .catch(function (error) {
      console.log(error);
    });
}
function setLocalStorage(mangSP) {
  localStorage.setItem("DSSP", JSON.stringify(mangSP));
}

function getLocalStorage() {
  let mangKQ = JSON.parse(localStorage.getItem("DSSP"));
  return mangKQ;
}

document.getElementById("btnThemSP").addEventListener("click", function () {
  let footerEle = document.querySelector(".modal-footer");
  footerEle.innerHTML = `
        <button onclick="addProducts()" class="btn btn-success">Add Product</button>
    `;
});

function renderTable(mangSP) {
  let contentHTML = "";
  let count = 1;
  mangSP.map(function (sp, index) {
    contentHTML += `
            <tr>
                <td>${sp.id}</td>
                <td>${sp.name}</td>
                <td>${sp.price}</td>
                <td>${sp.img}</td>
                <td>${sp.desc}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaSP('${sp.id}')">Xóa</button>
                    <button class="btn btn-info" onclick="xemSP('${sp.id}')">Xem</button>
                </td>
            </tr>
        `;
    count++;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}

function addProducts() {
  //B1: Lấy thông tin(info) từ form
  // data, info
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let img = document.getElementById("HinhSP").value;
  let desc = document.getElementById("MoTa").value;

  let sp = new SanPham(name, price, img, desc);
  console.log("sp: ", sp);

  //B2: lưu info xuống database(cơ sở dữ liệu)
  sanPham
    .themSP(sp)
    .then(function (result) {
      //Load lại danh sách sau khi thêm thành công
      getListProducts();

      var valid = true;

      //Kiểm tra rỗng
      valid &=
        kiemTraRong(name, "#tbTenSPEmpty", "Tên sản phẩm") &
        kiemTraRong(price, "#tbGiaSPEmpty", "Gía sản phẩm") &
        kiemTraRong(img, "#tbHinhAnhSPEmpty", "Hình ảnh") &
        kiemTraRong(desc, "#tbMoTaSPEmpty", "Mô tả");

      //Kiểm tra chữ số
      valid &= kiemTraNumber(price, "#tbGiaSPNumber", "Gía sản phẩm");

      if (!valid) {
        return;
      }

      //gọi sự kiên click có sẵn của close button
      //Để tắt modal khi thêm thành công
      document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
      console.log(error);
    });
}

function xoaSP(id) {
  sanPham
    .xoaSanPham(id)
    .then(function (result) {
      //Load lại danh sách sau khi xóa thành công
      getListProducts();
    })
    .catch(function (error) {
      console.log(error);
    });
}

function xemSP(id) {
  sanPham
    .xemSanPham(id)
    .then(function (result) {
      console.log(result.data);
      //Mở modal
      $("#myModal").modal("show");
      //Điền thông tin lên form
      document.getElementById("TenSP").value = result.data.name;
      document.getElementById("GiaSP").value = result.data.price;
      document.getElementById("HinhSP").value = result.data.img;
      document.getElementById("MoTa").value = result.data.desc;

      //Thêm button cập nhật cho form
      let footerEle = document.querySelector(".modal-footer");
      footerEle.innerHTML = `
            <button onclick="capNhatSP('${result.data.id}')" class="btn btn-success">Update Product</button>
        `;
    })
    .catch(function (error) {
      console.log(error);
    });
}

function capNhatSP(id) {
  //B1: Lấy thông tin(info) từ form
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let img = document.getElementById("HinhSP").value;
  let desc = document.getElementById("MoTa").value;

  let sp = new SanPham(name, price, img, desc);
  console.log(sp);

  //B2: Cập nhật thông tin mới xuống DB
  sanPham
    .capNhatSanPham(id, sp)
    .then(function (result) {
      console.log(result.data);
      //Load lại danh sách sau khi cập nhật thành công
      getListProducts();

      //gọi sự kiên click có sẵn của close button
      //Để tắt modal khi cập nhật thành công
      document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
      console.log(error);
    });
}

//Kiểm tra vadilation
